# Contracts in depth

## Actor model
The actor model is a design pattern firstly inroduced in Erlang programming language. The idea is that each process is separate from other processes and communicates with them by asynchronous message sending. Contracts in cosmwasm use similar idea. In this model actor can be described something like that:
```rust
pub trait Actor {
    fn handle(msgPayload: &[u8]) -> Vec<Msg>;
}

pub struct Msg {
    pub destination: Vec<u8>,
    pub payload: Vec<u8>,
}
```
So a contract does not directly send messages but instead returns them. It's make sense since contract as opposed to Erlang process should not constantly exists in backround but rather finish as soon as possible.   
This model brings alot of good things in security and extensibility for contracts. Every contract is isolated and can run in parallel. Absence of direct calls completely remove possibility of reentrance attacks. Also this approach allows for inter blockchain messaging using IBC protocol.
The downside of the classic actor model is a requirement of quite complicated synchronization mechanisms in a distribured system (such as three phase commit). But since all contracts run in the same environment in the same process it is possible to atomicly execute contracts. So if all messages in a sequence are processed correctly, the state changes will be commited, but if an error occures state updates will be reverted without any extra actions from contracts code.

## Queries
Not every interaction between contracts results in state updates. For read only interactions there is mechanism of queries which is differ from messaging. Contracts can execute a raw query with help of `query_raw` function in order to access other contract storage directly and read raw data. But this isn't convenient because contract needs to know how exactly another contract storage works. More reliable way is to use custom query (`query_custom` function). A contract can send another contract payload with custom data which then processed in `query` entry point in the callee contract and return custom response. It is important to note that query has access to the state right before current message execution, so any updates made by currently executing contract is invisible to the query handler code.   
The internal (by a contract) query calls use gas as normal, it's a part of contract execution. But external queries made from CLI or from web apps, as far as I know, currently does not use any gas and can run forever which can be quite dangerous for some public nodes.

## Contracts in Rust
The contract usually defines 3 or more entry points. The main entry points are `instantiate`, `execute` and `query`.
`execute` entry point looks something like this:
```rust
pub fn execute(deps: DepsMut, env: Env, info: MessageInfo, msg: ExecuteMsg) -> Result<Response, ContractError>
```
where `ExecuteMsg` is defined by user and the rest of types defined in cosmwasm library. Other entry points have mostly the same structure.
The `query` entry point is a little bit different, because it's read only.
```rust
pub fn query(deps: Deps, env: Env, msg: QueryMsg) -> Result<Binary, ContractError>
```
There is a pattern here: all functions return `Result<Something, ContractError>`. This is a common pattern in Rust. Type `Result`:
```rust
enum Result<T, E> {
    Ok(T),
    Err(E),
}
```
is a standard type for returning errors. There is also `StdResult` which is just an alias for `Result` with `StdError`:
```rust
type StdResult<T> = Result<T, StdError>;
```
`StdError` is an enum with standard errors: overflow or divide by zero for example. And `ContractError` is a custom enum type which must contain `StdError` and may also contain specific errors for the contract.   
A contract can contain only one entry point of each type. In order to handle different actions with only one entry point, this entry point should dispatch and process different types of messages. Usually it's achieved by making an enum type and using pattern matching in entry point to call the corresponding handler.   
Entry points (except for `query`) returns `Response` (wrapped in `Result`) declared in cosmwasm library:
```rust
pub struct Response<T = Empty>
{
    /// Optional list of "subcalls" to make. These will be executed in order
    /// (and this contract's subcall_response entry point invoked)
    /// *before* any of the "fire and forget" messages get executed.
    pub submessages: Vec<SubMsg<T>>,
    /// After any submessages are processed, these are all dispatched in the host blockchain.
    /// If they all succeed, then the transaction is committed. If any fail, then the transaction
    /// and any local contract state changes are reverted.
    pub messages: Vec<CosmosMsg<T>>,
    /// The attributes that will be emitted as part of a "wasm" event
    pub attributes: Vec<Attribute>,
    pub data: Option<Binary>,
}
```
Generic type `T` here is for custom message types. And it's empty if there is no need in custom messages. There are different vaiants and types of messages as declared here:
```rust
pub enum CosmosMsg<T = Empty>
{
    Bank(BankMsg),
    /// This can be defined by each blockchain as a custom extension
    Custom(T),
    Staking(StakingMsg),
    Distribution(DistributionMsg),
    Stargate {
        type_url: String,
        value: Binary,
    },
    Ibc(IbcMsg),
    Wasm(WasmMsg),
}
```
A submessage is just a message but with a request for the blockchain to reply after submessage is processed:
```rust
pub struct SubMsg<T = Empty>
{
    pub id: u64,
    pub msg: CosmosMsg<T>,
    pub gas_limit: Option<u64>,
    pub reply_on: ReplyOn,
}

pub enum ReplyOn {
    /// Always perform a callback after SubMsg is processed
    Always,
    /// Only callback if SubMsg returned an error, no callback on success case
    Error,
    /// Only callback if SubMsg was successful, no callback on error case
    Success,
}
```
Every incoming message is deserialized from JSON and every outgoing response is serialized to JSON. It's done in the contract code with help of a de-facto standard library called `serde`.

## Low level details
A contract code compiled in wasm module, this module then executed by the vm in the blockchain with interaction through set of imported and exported functions. But a contract has it's own types defined both in libraries and in the contract code itself, so the wasm vm and the blockchain can not possibly know internal structure of those types. This is why there is a need for JSON serialization. But the result of this serialization is Rust type `String` and execution environment doesn't know about this type either. More so Rust doesn't give any guarantees about internal structure of standard types like `String` or `Vec`. And even more so contract can be written in any language with a compiler for wasm. In other words all types must be at C-like level: only primitive types, pointers and structs. So the cosmwasm library for Rust provides all necessary low level functions and types and also high level abstractions which hide all this low level details.   
For example all imports look like this:
```rust
extern "C" {
    fn db_read(key: u32) -> u32;
    fn db_write(key: u32, value: u32);
    fn db_remove(key: u32);

    fn db_scan(start_ptr: u32, end_ptr: u32, order: i32) -> u32;
    fn db_next(iterator_id: u32) -> u32;

    fn addr_validate(source_ptr: u32) -> u32;
    fn addr_canonicalize(source_ptr: u32, destination_ptr: u32) -> u32;
    fn addr_humanize(source_ptr: u32, destination_ptr: u32) -> u32;
    fn secp256k1_verify(message_hash_ptr: u32, signature_ptr: u32, public_key_ptr: u32) -> u32;
    fn secp256k1_recover_pubkey(message_hash_ptr: u32, signature_ptr: u32, recovery_param: u32) -> u64;
    fn ed25519_verify(message_ptr: u32, signature_ptr: u32, public_key_ptr: u32) -> u32;
    fn ed25519_batch_verify(messages_ptr: u32, signatures_ptr: u32, public_keys_ptr: u32) -> u32;
    fn debug(source_ptr: u32);
    fn query_chain(request: u32) -> u32;
}
```
and exports:
```rust
extern "C" fn interface_version_8() -> ();
extern "C" fn allocate(size: usize) -> u32;
extern "C" fn deallocate(pointer: u32);

extern "C" fn instantiate(env_ptr: u32, info_ptr: u32, msg_ptr: u32) -> u32;
extern "C" fn execute(env_ptr: u32, info_ptr: u32, msg_ptr: u32) -> u32;
extern "C" fn query(env_ptr: u32, msg_ptr: u32) -> u32;
```
And I think the most interesting detail in this is pointers which in types are just `u32`. But in reallity they are more like `*mut Region` where `Region` type is:
```rust
/// Describes some data allocated in Wasm's linear memory.
/// A pointer to an instance of this can be returned over FFI boundaries.
///
/// This struct is crate internal since the cosmwasm-vm defines the same type independently.
#[repr(C)]
pub struct Region {
    /// The beginning of the region expressed as bytes from the beginning of the linear memory
    pub offset: u32,
    /// The number of bytes available in this region
    pub capacity: u32,
    /// The number of bytes used in this region
    pub length: u32,
}
```
But wasm vm doesn't know much about memory and regions. It just allocates 32mb total per loaded wasm module. Next this memory managed by a contract and it's allocator. So this is why contract must export `allocate` and `deallocate` functions.   
So usual workflow of a contract looks something like this: 
1. vm allocates whole memory buffer for a wasm module.
2. starts the module
3. calls `allocate` to allocate necessary regions in wasm address space
4. copies messages and other info to this regions in wasm memory
5. calls `execute` entry point for example
6. copies response data from wasm memory back to vm memory
7. calls `deallocate` to free used regions
On Rust and the cosmwasm library side of things there is a macro called `entry_point` which do something like that:
```rust
pub fn execute(deps: DepsMut, env: Env, info: MessageInfo, msg: ExecuteMsg) -> Result<Response, ContractError> {
    // execute code here
}

mod wasm_export {
    extern "C" fn execute(env_ptr: u32, info_ptr: u32, msg_ptr: u32) -> u32 {
        cosmwasm_std::do_execute(&super::{name}, env_ptr, info_ptr, msg_ptr)
    }
}
```
So this macro generates export function and in this function calls a standard wrapper with address of `execute` contract function. This wrapper then perform deserialization of parameters and serialization of a result.
