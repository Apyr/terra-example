mod coins;

use crate::ContractError;
pub use coins::Coins;
use cosmwasm_std::{Addr, StdResult, Storage};
use cw_storage_plus::{Item, Map};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

pub type Time = u64; // timestamp (in ms)

const MS_PER_DAY: u64 = 24 * 60 * 60 * 1000;

fn days_between(begin: Time, end: Time) -> u64 {
    if end <= begin {
        0
    } else {
        (end - begin) / MS_PER_DAY
    }
}

const STATE: Item<State> = Item::new("state");
const EMPLOYEES_MAP: Map<&Addr, Employee> = Map::new("employees");

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, JsonSchema)]
pub struct Employee {
    pub address: Addr,
    pub name: String,
    pub hiring_date: Time,
    pub firing_date: Option<Time>,
    pub daily_rate: Coins,
    pub last_pay_time: Time,
    pub payed: Coins,
}

impl Employee {
    pub fn new(address: Addr, name: String, hiring_date: Time, daily_rate: Coins) -> Employee {
        Employee {
            address,
            name,
            hiring_date,
            firing_date: None,
            daily_rate,
            last_pay_time: hiring_date,
            payed: Coins::default(),
        }
    }

    pub fn try_load(store: &dyn Storage, addr: &Addr) -> StdResult<Option<Employee>> {
        EMPLOYEES_MAP.may_load(store, addr)
    }

    pub fn load(store: &dyn Storage, addr: &Addr) -> Result<Employee, ContractError> {
        let opt = Self::try_load(store, addr)?;
        if let Some(employee) = opt {
            Ok(employee)
        } else {
            Err(ContractError::EmployeeNotFound {
                address: addr.clone(),
            })
        }
    }

    pub fn save(&self, store: &mut dyn Storage) -> StdResult<()> {
        EMPLOYEES_MAP.save(store, &self.address, self)
    }

    pub fn fire(&mut self, firing_date: Time) {
        self.firing_date = Some(firing_date);
    }

    pub fn is_fired(&self) -> bool {
        self.firing_date.is_some()
    }

    pub fn get_max_salary(&self, mut current_time: Time) -> Coins {
        if let Some(firing_date) = self.firing_date {
            current_time = firing_date;
        }
        let days = days_between(self.last_pay_time, current_time);
        self.daily_rate.clone() * days - &self.payed
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, JsonSchema)]
pub struct State {
    pub owner: Addr,
    pub employees_addrs: Vec<Addr>,
}

impl State {
    pub fn new(owner: Addr) -> State {
        State {
            owner,
            employees_addrs: vec![],
        }
    }

    pub fn save(&self, store: &mut dyn Storage) -> StdResult<()> {
        STATE.save(store, self)
    }

    pub fn load(store: &dyn Storage) -> StdResult<State> {
        STATE.load(store)
    }

    pub fn add_employee(&mut self, employee: &Employee) {
        self.employees_addrs.push(employee.address.clone());
    }

    pub fn load_employees(&self, store: &dyn Storage) -> Result<Vec<Employee>, ContractError> {
        self.employees_addrs
            .iter()
            .map(|addr| Employee::load(store, addr))
            .collect()
    }
}
