use cosmwasm_std::{Coin, Uint128};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::{
    fmt::Display,
    ops::{Add, Mul, Sub},
};

// length of the internal vector usually will be very small (mostly 1 or 2)
// that's why it is better for perfomance to use a vector instead of a hash map
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq, JsonSchema)]
pub struct Coins(pub Vec<Coin>);

impl Coins {
    pub fn try_find(&self, denom: &str) -> Option<usize> {
        for (index, coin) in self.0.iter().enumerate() {
            if coin.denom == denom {
                return Some(index);
            }
        }
        None
    }

    pub fn find(&mut self, denom: &str) -> usize {
        if let Some(index) = self.try_find(denom) {
            index
        } else {
            self.0.push(Coin::new(0, denom));
            self.0.len() - 1
        }
    }

    pub fn get(&self, denom: &str) -> Uint128 {
        if let Some(index) = self.try_find(denom) {
            self.0[index].amount
        } else {
            Uint128::new(0)
        }
    }

    pub fn set(&mut self, denom: &str, amount: Uint128) {
        let index = self.find(denom);
        self.0[index].amount = amount;
    }

    pub fn update(&mut self, denom: &str, func: impl FnOnce(Uint128) -> Uint128) {
        let index = self.find(denom);
        self.0[index].amount = func(self.0[index].amount);
    }

    pub fn is_less_than(&self, coins: &Coins) -> bool {
        for coin in self.0.iter() {
            if coin.amount > coins.get(&coin.denom) {
                return false;
            }
        }
        true
    }
}

impl Display for Coins {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.0.is_empty() {
            write!(f, "")
        } else {
            write!(f, "{}", self.0[0])?;
            for coin in &self.0[1..] {
                write!(f, ", {}", coin)?;
            }
            Ok(())
        }
    }
}

impl From<Vec<Coin>> for Coins {
    fn from(coins: Vec<Coin>) -> Self {
        Coins(coins)
    }
}

impl Mul<u64> for Coins {
    type Output = Coins;

    fn mul(mut self, rhs: u64) -> Coins {
        for coin in self.0.iter_mut() {
            coin.amount *= Uint128::from(rhs as u128);
        }
        self
    }
}

impl Add<&Coins> for Coins {
    type Output = Coins;

    fn add(mut self, rhs: &Coins) -> Coins {
        for coin in rhs.0.iter() {
            self.update(&coin.denom, |amount| amount + coin.amount);
        }
        self
    }
}

impl Sub<&Coins> for Coins {
    type Output = Coins;

    fn sub(mut self, rhs: &Coins) -> Coins {
        for coin in rhs.0.iter() {
            self.update(&coin.denom, |amount| amount - coin.amount);
        }
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_display() {
        let coins = Coins(vec![Coin::new(10, "UST"), Coin::new(20, "LUNA")]);
        assert_eq!(coins.to_string(), "10UST, 20LUNA".to_owned());
    }

    #[test]
    fn test_add() {
        let coins1 = Coins(vec![Coin::new(10, "UST"), Coin::new(20, "LUNA")]);
        let coins2 = Coins(vec![Coin::new(5, "RUB")]);
        assert_eq!(
            coins1 + &coins2,
            Coins(vec![
                Coin::new(10, "UST"),
                Coin::new(20, "LUNA"),
                Coin::new(5, "RUB")
            ])
        );
    }

    #[test]
    fn test_sub() {
        let coins1 = Coins(vec![Coin::new(10, "UST"), Coin::new(20, "LUNA")]);
        let coins2 = Coins(vec![Coin::new(5, "UST")]);
        assert_eq!(
            coins1 - &coins2,
            Coins(vec![Coin::new(5, "UST"), Coin::new(20, "LUNA")])
        );
    }

    #[test]
    fn test_mul() {
        let coins = Coins(vec![Coin::new(5, "UST"), Coin::new(20, "LUNA")]);
        assert_eq!(
            coins * 3,
            Coins(vec![Coin::new(15, "UST"), Coin::new(60, "LUNA")])
        );
    }

    #[test]
    fn test_is_less_than() {
        let coins1 = Coins(vec![Coin::new(10, "UST"), Coin::new(20, "LUNA")]);
        let coins2 = Coins(vec![Coin::new(5, "UST")]);
        assert!(!coins1.is_less_than(&coins2));
        assert!(coins2.is_less_than(&coins1));
    }
}
