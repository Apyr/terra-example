use crate::state::Coins;
use cosmwasm_std::{Addr, StdError};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ContractError {
    #[error("{0}")]
    Std(#[from] StdError),

    #[error("Unauthorized")]
    Unauthorized {},

    #[error("EmployeeNotFound")]
    EmployeeNotFound { address: Addr },

    #[error("EmployeeAlreadyExists (address: {address})")]
    EmployeeAlreadyExists { address: Addr },

    #[error("MoreThanMaxSalary (salary: {salary}; max_salary: {max_salary})")]
    MoreThanMaxSalary { salary: Coins, max_salary: Coins },

    #[error("MoreThanMaxSalary (salary: {salary}; funds: {funds})")]
    NotEnoughFunds { salary: Coins, funds: Coins },
}
