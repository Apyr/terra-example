use crate::state::{Coins, Employee, Time};
use cosmwasm_std::Addr;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, JsonSchema)]
pub struct InstantiateMsg {}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, JsonSchema)]
#[serde(rename_all = "snake_case")]
pub enum ExecuteMsg {
    Hire {
        address: Addr,
        name: String,
        daily_rate: Coins,
    },
    Fire {
        address: Addr,
    },
    UpdateEmployee {
        address: Addr,
        name: String,
        daily_rate: Coins,
    },
    Pay {
        address: Option<Addr>,
        salary: Option<Coins>,
    },
    // for test purposes only
    SetHiringDate {
        address: Addr,
        hiring_date: Time,
    },
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, JsonSchema)]
#[serde(rename_all = "snake_case")]
pub enum QueryMsg {
    GetEmployees {},
    GetMaxSalary { address: Addr },
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, JsonSchema)]
pub struct GetEmployeesResponse {
    pub employees: Vec<Employee>,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, JsonSchema)]
pub struct GetMaxSalaryResponse {
    pub max_salary: Coins,
}
