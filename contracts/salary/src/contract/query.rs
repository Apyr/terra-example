use crate::{
    msg::{GetEmployeesResponse, GetMaxSalaryResponse},
    state::{Employee, State},
    ContractError,
};
use cosmwasm_std::{Addr, Deps, Env, StdResult};

pub struct Query<'a> {
    deps: Deps<'a>,
    env: Env,
    state: State,
}

impl<'a> Query<'a> {
    pub fn new(deps: Deps<'a>, env: Env) -> StdResult<Self> {
        Ok(Query {
            state: State::load(deps.storage)?,
            deps,
            env,
        })
    }

    pub fn get_employees(&self) -> Result<GetEmployeesResponse, ContractError> {
        let employees = self.state.load_employees(self.deps.storage)?;
        Ok(GetEmployeesResponse { employees })
    }

    pub fn get_max_salary(&self, address: Addr) -> Result<GetMaxSalaryResponse, ContractError> {
        let employee = Employee::load(self.deps.storage, &address)?;
        let max_salary = employee.get_max_salary(self.env.block.time.nanos() / 1000_000);
        Ok(GetMaxSalaryResponse { max_salary })
    }
}
