use crate::{
    state::{Coins, Employee, State, Time},
    ContractError,
};
use cosmwasm_std::{Addr, BankMsg, DepsMut, Env, MessageInfo, Response, StdResult};

pub struct Execute<'a> {
    deps: DepsMut<'a>,
    env: Env,
    info: MessageInfo,
    state: State,
}

impl<'a> Execute<'a> {
    pub fn new(deps: DepsMut<'a>, env: Env, info: MessageInfo) -> StdResult<Self> {
        Ok(Execute {
            state: State::load(deps.storage)?,
            deps,
            env,
            info,
        })
    }

    fn check_owner(&self) -> Result<(), ContractError> {
        if self.info.sender != self.state.owner {
            Err(ContractError::Unauthorized {})
        } else {
            Ok(())
        }
    }

    fn now(&self) -> Time {
        self.env.block.time.nanos() / 1000_000
    }

    pub fn hire(
        &mut self,
        address: Addr,
        name: String,
        daily_rate: Coins,
    ) -> Result<Response, ContractError> {
        self.check_owner()?;
        let opt = Employee::try_load(self.deps.storage, &address)?;
        if opt.is_some() {
            return Err(ContractError::EmployeeAlreadyExists { address });
        }

        let hiring_date = self.now();
        let employee = Employee::new(address, name, hiring_date, daily_rate);
        employee.save(self.deps.storage)?;
        self.state.add_employee(&employee);
        self.state.save(self.deps.storage)?;

        Ok(Response::new())
    }

    pub fn fire(&mut self, address: Addr) -> Result<Response, ContractError> {
        self.check_owner()?;

        let mut employee = Employee::load(self.deps.storage, &address)?;
        let firing_date = self.now();
        employee.fire(firing_date);
        employee.save(self.deps.storage)?;

        Ok(Response::default())
    }

    pub fn update_employee(
        &mut self,
        address: Addr,
        name: String,
        daily_rate: Coins,
    ) -> Result<Response, ContractError> {
        self.check_owner()?;

        let mut employee = Employee::load(self.deps.storage, &address)?;
        employee.name = name;
        employee.daily_rate = daily_rate;
        employee.save(self.deps.storage)?;

        Ok(Response::default())
    }

    pub fn pay(
        &mut self,
        address: Option<Addr>,
        salary: Option<Coins>,
    ) -> Result<Response, ContractError> {
        let addr = if let Some(addr) = address {
            self.check_owner()?; // only the owner can pay another people
            addr
        } else {
            self.info.sender.clone()
        };

        let mut employee = Employee::load(self.deps.storage, &addr)?;
        let mut max_salary = employee.get_max_salary(self.now());
        if let Some(salary) = salary {
            if !salary.is_less_than(&max_salary) {
                // the order of comparison is important
                return Err(ContractError::MoreThanMaxSalary { salary, max_salary });
            }
            max_salary = salary;
        }

        let funds = Coins(
            self.deps
                .querier
                .query_all_balances(self.env.contract.address.clone())?,
        );
        if !max_salary.is_less_than(&funds) {
            return Err(ContractError::NotEnoughFunds {
                salary: max_salary,
                funds,
            });
        }

        employee.payed = employee.payed + &max_salary;
        employee.save(self.deps.storage)?;

        Ok(Response::new().add_message(BankMsg::Send {
            to_address: addr.into_string(),
            amount: max_salary.0,
        }))
    }

    pub fn set_hiring_date(
        &mut self,
        address: Addr,
        hiring_date: Time,
    ) -> Result<Response, ContractError> {
        self.check_owner()?;

        let mut employee = Employee::load(self.deps.storage, &address)?;
        employee.hiring_date = hiring_date;
        employee.save(self.deps.storage)?;

        Ok(Response::default())
    }
}
