mod execute;
mod query;

use self::execute::Execute;
use self::query::Query;
use crate::error::ContractError;
use crate::msg::{ExecuteMsg, InstantiateMsg, QueryMsg};
use crate::state::State;
#[cfg(not(feature = "library"))]
use cosmwasm_std::entry_point;
use cosmwasm_std::{to_binary, Binary, Deps, DepsMut, Env, MessageInfo, Response};
use cw2::set_contract_version;

// version info for migration info
const CONTRACT_NAME: &str = "salary";
const CONTRACT_VERSION: &str = env!("CARGO_PKG_VERSION");

#[cfg_attr(not(feature = "library"), entry_point)]
pub fn instantiate(
    deps: DepsMut,
    _env: Env,
    info: MessageInfo,
    _msg: InstantiateMsg,
) -> Result<Response, ContractError> {
    set_contract_version(deps.storage, CONTRACT_NAME, CONTRACT_VERSION)?;

    let state = State::new(info.sender.clone());
    state.save(deps.storage)?;

    Ok(Response::default())
}

#[cfg_attr(not(feature = "library"), entry_point)]
pub fn execute(
    deps: DepsMut,
    env: Env,
    info: MessageInfo,
    msg: ExecuteMsg,
) -> Result<Response, ContractError> {
    let mut exe = Execute::new(deps, env, info)?;
    match msg {
        ExecuteMsg::Hire {
            address,
            name,
            daily_rate,
        } => exe.hire(address, name, daily_rate),
        ExecuteMsg::Fire { address } => exe.fire(address),
        ExecuteMsg::UpdateEmployee {
            address,
            name,
            daily_rate,
        } => exe.update_employee(address, name, daily_rate),
        ExecuteMsg::Pay { address, salary } => exe.pay(address, salary),
        ExecuteMsg::SetHiringDate {
            address,
            hiring_date,
        } => exe.set_hiring_date(address, hiring_date),
    }
}

#[cfg_attr(not(feature = "library"), entry_point)]
pub fn query(deps: Deps, env: Env, msg: QueryMsg) -> Result<Binary, ContractError> {
    let query = Query::new(deps, env)?;
    let result = match msg {
        QueryMsg::GetEmployees {} => to_binary(&query.get_employees()?)?,
        QueryMsg::GetMaxSalary { address } => to_binary(&query.get_max_salary(address)?)?,
    };
    Ok(result)
}

#[cfg(test)]
mod tests {
    use crate::msg::{GetEmployeesResponse, GetMaxSalaryResponse};

    use super::*;
    use cosmwasm_std::testing::{mock_dependencies, mock_env, mock_info};
    use cosmwasm_std::{coins, from_binary, Addr, BankMsg, CosmosMsg};

    #[test]
    fn test_init() {
        let mut deps = mock_dependencies(&[]);
        let info = mock_info("creator", &coins(1000, "luna"));

        assert!(instantiate(deps.as_mut(), mock_env(), info, InstantiateMsg {}).is_ok());
    }

    #[test]
    fn test_hire() {
        let mut deps = mock_dependencies(&[]);
        let in_info = mock_info("creator", &coins(1000, "luna"));
        let info = mock_info("creator", &[]);
        let mut env = mock_env();

        instantiate(deps.as_mut(), env.clone(), in_info, InstantiateMsg {}).unwrap();

        let msg = ExecuteMsg::Hire {
            address: Addr::unchecked("empl1"),
            name: "Employee1".into(),
            daily_rate: coins(5, "luna").into(),
        };
        execute(deps.as_mut(), env.clone(), info.clone(), msg).unwrap();

        env.block.time = env.block.time.plus_seconds(200);

        let msg = ExecuteMsg::Hire {
            address: Addr::unchecked("empl2"),
            name: "Employee2".into(),
            daily_rate: coins(7, "luna").into(),
        };
        execute(deps.as_mut(), env.clone(), info.clone(), msg).unwrap();

        let res = query(deps.as_ref(), env.clone(), QueryMsg::GetEmployees {}).unwrap();
        let res: GetEmployeesResponse = from_binary(&res).unwrap();

        assert_eq!(res.employees.len(), 2);
        assert_eq!(res.employees[0].name, "Employee1".to_owned());
        assert_eq!(res.employees[1].name, "Employee2".to_owned());
        assert_eq!(res.employees[0].address.to_string(), "empl1".to_owned());
        assert_eq!(res.employees[1].address.to_string(), "empl2".to_owned());
        assert_eq!(res.employees[0].daily_rate.0, coins(5, "luna"));
        assert_eq!(res.employees[1].daily_rate.0, coins(7, "luna"));

        assert_eq!(
            res.employees[1].hiring_date - res.employees[0].hiring_date,
            200 * 1000
        );
    }

    #[test]
    fn test_pay() {
        let mut deps = mock_dependencies(&coins(1000, "luna"));
        let info = mock_info("creator", &[]);
        let mut env = mock_env();

        instantiate(deps.as_mut(), env.clone(), info.clone(), InstantiateMsg {}).unwrap();

        let msg = ExecuteMsg::Hire {
            address: Addr::unchecked("empl1"),
            name: "Employee1".into(),
            daily_rate: coins(5, "luna").into(),
        };
        execute(deps.as_mut(), env.clone(), info.clone(), msg).unwrap();

        env.block.time = env.block.time.plus_seconds(200);

        let msg = ExecuteMsg::Hire {
            address: Addr::unchecked("empl2"),
            name: "Employee2".into(),
            daily_rate: coins(7, "luna").into(),
        };
        execute(deps.as_mut(), env.clone(), info.clone(), msg).unwrap();

        env.block.time = env.block.time.plus_seconds(7 * 24 * 60 * 60);
        let msg = QueryMsg::GetMaxSalary {
            address: Addr::unchecked("empl1"),
        };
        let res = query(deps.as_ref(), env.clone(), msg).unwrap();
        let res: GetMaxSalaryResponse = from_binary(&res).unwrap();
        assert_eq!(res.max_salary.0, coins(35, "luna"));

        let msg = ExecuteMsg::Pay {
            address: Some(Addr::unchecked("empl1")),
            salary: Some(coins(30, "luna").into()),
        };
        let result = execute(deps.as_mut(), env.clone(), info.clone(), msg).unwrap();
        let msg = CosmosMsg::Bank(BankMsg::Send {
            to_address: "empl1".to_string(),
            amount: coins(30, "luna"),
        });
        assert_eq!(result.messages[0].msg, msg);
    }
}
