// can use `process.env.SECRET_MNEMONIC` or `process.env.SECRET_PRIV_KEY`
// to populate secret in CI environment instead of hardcoding

module.exports = {
  test: {
    mnemonic: 'demand dumb cradle slab holiday check sting napkin soap width country sadness property gesture taste miss test bundle final father friend flame scatter grief'
  },
};
